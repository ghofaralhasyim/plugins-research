import Vue from 'vue'
import VueHighlightJS from 'vue-highlightjs'
import 'highlight.js/styles/github-dark.css'

Vue.use(VueHighlightJS, {
	// Register only languages that you want
})
